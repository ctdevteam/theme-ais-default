import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidebarComponent } from './view/layout/sidebar/sidebar.component';
import { HomeComponent } from './view/page/home/home.component';

const routes: Routes = [
  { path: '*', redirectTo: '/', pathMatch: 'full' },
  {path: '' , component: SidebarComponent ,
    canActivate: [],
    children: [
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      {path: 'home' , component: HomeComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
