import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
  }
  public isLinkActive(url: string): boolean {
    return "/" + this.route.snapshot.url.join("/") === url;
  }
}
