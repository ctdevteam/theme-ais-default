export const environment = {
  production: true,
  url_api: 'http://128.199.167.160:3055',
  app_name: 'trans-b',
  MappingMessage:{
    "200":{
      "GET":{
        code:"200",
        title:"Success",
        status:"Your data has been successfully."
      },
      "POST":{
        code:"200",
        title:"Success",
        status:"Your data has been created."
      },
      "PUT":{
        code:"200",
        title:"Success",
        status:"Your data has been updated."
      },"DELETE":{
        code:"200",
        title:"Success",
        status:"Your data has been deleted."
      }
    },
    "201":{
      "POST":{
        code:"201",
        title:"Success",
        status:"Your data has been created."
      }
    },
    "400":{
      "GET":{
        code:"400",
        title:"Error",
        status:"Bad Request"
      },
      "POST":{
        code:"400",
        title:"Error",
        status:"Bad Request"
      },
      "PUT":{
        code:"400",
        title:"Error",
        status:"Bad Request"
      },"DELETE":{
        code:"400",
        title:"Error",
        status:"Bad Request"
      }
    },
    "401":{
      "GET":{
        code:"401",
        title:"Error",
        status:"Unauthorized"
      },
      "POST":{
        code:"401",
        title:"Error",
        status:"Unauthorized"
      },
      "PUT":{
        code:"401",
        title:"Error",
        status:"Unauthorized"
      },"DELETE":{
        code:"401",
        title:"Error",
        status:"Unauthorized"
      }
    },
    "404":{
      "GET":{
        code:"404",
        title:"Warning",
        status:"Data Not Found."
      },
      "POST":{
        code:"404",
        title:"Warning",
        status:"Data Not Found."
      },
      "PUT":{
        code:"404",
        title:"Warning",
        status:"Data Not Found."
      },"DELETE":{
        code:"404",
        title:"Warning",
        status:"Data Not Found."
      }
    },
    "409":{
      "GET":{
        code:"409",
        title:"Warning",
        status:"The data is already exist"
      },
      "POST":{
        code:"409",
        title:"Warning",
        status:"The data is already exist"
      },
      "PUT":{
        code:"409",
        title:"Warning",
        status:"The data is already exist"
      },"DELETE":{
        code:"409",
        title:"Warning",
        status:"The data is already in use"
      }
    },
    "500":{
      "GET":{
        code:"500",
        title:"Error",
        status:"Internal Server Error"
      },
      "POST":{
        code:"500",
        title:"Error",
        status:"Internal Server Error"
      },
      "PUT":{
        code:"500",
        title:"Error",
        status:"Internal Server Error"
      },"DELETE":{
        code:"500",
        title:"Error",
        status:"Internal Server Error"
      }
    },
    "001":{
      "SELECT":{
        code:"001",
        title:"Warning",
        status:"Invalid file Type"
      },
    },
    "002":{
      "POST":{
        code:"002",
        title:"Success",
        status:"Already Joined App"
      },
    },
  }
};
